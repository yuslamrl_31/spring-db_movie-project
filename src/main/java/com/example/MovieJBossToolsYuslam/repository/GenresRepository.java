package com.example.MovieJBossToolsYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.MovieJBossToolsYuslam.entities.Genres;

public interface GenresRepository extends JpaRepository<Genres, Long> {

}
