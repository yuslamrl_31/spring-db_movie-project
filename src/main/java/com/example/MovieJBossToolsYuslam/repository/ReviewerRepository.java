package com.example.MovieJBossToolsYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.MovieJBossToolsYuslam.entities.Reviewer;

public interface ReviewerRepository extends JpaRepository<Reviewer, Long>{

}
