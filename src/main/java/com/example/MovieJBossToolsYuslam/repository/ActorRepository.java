package com.example.MovieJBossToolsYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.MovieJBossToolsYuslam.entities.Actor;


public interface ActorRepository extends JpaRepository<Actor, Long > {

}
