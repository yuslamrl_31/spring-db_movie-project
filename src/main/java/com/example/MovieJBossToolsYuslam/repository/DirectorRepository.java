package com.example.MovieJBossToolsYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.MovieJBossToolsYuslam.entities.Director;

public interface DirectorRepository extends JpaRepository<Director, Long> {

}
