package com.example.MovieJBossToolsYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.MovieJBossToolsYuslam.entities.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long> {

}
