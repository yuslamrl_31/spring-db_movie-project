package com.example.MovieJBossToolsYuslam.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.example.MovieJBossToolsYuslam.repository.MovieRepository;
import com.example.MovieJBossToolsYuslam.dto.MovieDto;
import com.example.MovieJBossToolsYuslam.entities.Movie;


@RestController
@RequestMapping("/api")

public class MovieController {

	@Autowired
	MovieRepository movieRepository;
	
	//Get All Movie
	@GetMapping("/read/movie")
	public HashMap<String, Object> readMovie(){
		HashMap<String, Object> hmMovie = new HashMap<String, Object>();
		List<MovieDto> movieDtos = new ArrayList<MovieDto>();
		for(Movie movie : movieRepository.findAll()) {
			MovieDto movieDto = new MovieDto(movie.getMovId(), movie.getMovTitle(), movie.getMovYear(), movie.getMovTime(), movie.getMovLang(), movie.getMovDtRel(), movie.getMovRelCountry());
			movieDtos.add(movieDto);
		}
			
		hmMovie.put("Message : ", "Read Movie Succes");
		hmMovie.put("Total : ", movieDtos.size());
		hmMovie.put("Data : ", movieDtos);
			
		return hmMovie;
			
	}
	
	//Create Movie
	@PostMapping("/create/movie")
	public HashMap<String, Object> createMovie(@Valid @RequestBody MovieDto movieDtoDetails){
		HashMap<String, Object> hmMovie = new HashMap<String, Object>();
		Movie movie = new Movie(movieDtoDetails.getMovId(), movieDtoDetails.getMovTitle(), movieDtoDetails.getMovYear(), movieDtoDetails.getMovTime(), movieDtoDetails.getMovLang(), movieDtoDetails.getMovDtRel(), movieDtoDetails.getMovRelCountry());
			
		hmMovie.put("Message : ", "Create Movie Succes");
		hmMovie.put("Data : ", movieRepository.save(movie));
			
		return hmMovie;
		
	}
	
	//Update Movie
	@PutMapping("/update/movie/{id}")
	public HashMap<String, Object> updateMovie(@PathVariable(value = "id") Long id, @Valid @RequestBody MovieDto movieDtoDetails ){
		HashMap<String, Object> hmMovie = new HashMap<String, Object>();
		Movie movie = movieRepository.findById(id).orElseThrow(null);
			
		if(movieDtoDetails.getMovTitle() != null) {
			movie.setMovTitle(movieDtoDetails.getMovTitle());
		}
			
		if(movieDtoDetails.getMovYear() != 0) {
			movie.setMovYear(movieDtoDetails.getMovYear());
		}
			
		if(movieDtoDetails.getMovTime() != 0) {
			movie.setMovTime(movieDtoDetails.getMovTime());
		}
			
		if(movieDtoDetails.getMovLang() != null ) {
			movie.setMovLang(movieDtoDetails.getMovLang());
		}
			
		if(movieDtoDetails.getMovDtRel() != null) {
			movie.setMovDtRel(movieDtoDetails.getMovDtRel());
		}
			
		if(movieDtoDetails.getMovRelCountry() != null ) {
			movie.setMovRelCountry(movieDtoDetails.getMovRelCountry());
		}
			
		movieRepository.save(movie);
			
		hmMovie.put("Message : ", "Update Movie Succes");
		hmMovie.put("Data", movie);
			
		return hmMovie;
			
			
	}
	
	//Delete Movie
	@DeleteMapping("/delete/movie/{id}")
	public HashMap<String, Object> deleteMovie(@PathVariable (value = "id") Long id){
		HashMap<String, Object> hmMovie = new HashMap<String, Object>();
		Movie movie = movieRepository.findById(id).orElseThrow(null);
			
		movieRepository.delete(movie);
			
		hmMovie.put("Message : ", "Delete Movie Succes");
		hmMovie.put("Data : ", movie);
			
		return hmMovie;
			
	}
}
