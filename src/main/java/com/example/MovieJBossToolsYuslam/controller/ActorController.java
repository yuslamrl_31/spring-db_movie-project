
package com.example.MovieJBossToolsYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.MovieJBossToolsYuslam.repository.ActorRepository;
import com.example.MovieJBossToolsYuslam.dto.ActorDto;
import com.example.MovieJBossToolsYuslam.entities.Actor;

@RestController
@RequestMapping("/api")
public class ActorController {
		
	@Autowired
	ActorRepository actorRepository;
	
	//Get All Actor
	@GetMapping("/read/actor")
	public HashMap<String, Object> readActor(){
		HashMap<String, Object> hmActor = new HashMap<String, Object>();
		List<ActorDto> actorDtos = new ArrayList<ActorDto>();
		for(Actor actor : actorRepository.findAll()) {
			ActorDto actorDto = new ActorDto(actor.getActId(), actor.getActFname(), actor.getActLname(), actor.getActGender());
			actorDtos.add(actorDto);
		}
		
		hmActor.put("Message : ", "Read All Actor Success");
		hmActor.put("Total : ", actorDtos.size());
		hmActor.put("Data : " , actorDtos);
		
		return hmActor;
	}
	
	//Create Actor
	@PostMapping("/create/actor")
	public HashMap<String, Object> createActor(@Valid @RequestBody ActorDto actorDtoDetails){
		HashMap<String, Object> hmActor = new HashMap<String, Object>();
		Actor actor = new Actor(actorDtoDetails.getActId(), actorDtoDetails.getActFname(), actorDtoDetails.getActLname(), actorDtoDetails.getActGender());
			
		hmActor.put("Message : ", "Create Actor Succes");
		hmActor.put("Data : ", actorRepository.save(actor));
			
			return hmActor;
	}

	//Update Actor
	@PutMapping("/update/actor/{id}")
	public HashMap<String, Object> updateActor(@PathVariable(value = "id") Long id,@Valid @RequestBody ActorDto actorDtoDetails){
		HashMap<String, Object> hmActor = new HashMap<String, Object>();
		Actor actor = actorRepository.findById(id).orElseThrow(null);
			
		if(actorDtoDetails.getActFname() != null) {
			actor.setActFname(actorDtoDetails.getActFname());
		}
		if(actorDtoDetails.getActLname() != null ) {
			actor.setActLname(actorDtoDetails.getActLname());
		}
		if(actorDtoDetails.getActGender() != 0) {
			actor.setActGender(actorDtoDetails.getActGender());
		}
			
		actorRepository.save(actor);
			
		hmActor.put("Message : ", "Updated Actor Succes");
		hmActor.put("Data : ", actor);
			
		return hmActor;
			
		}
	
	//Delete Actor
	@DeleteMapping("/delete/actor/{id}")
	public HashMap<String, Object> deleteActor(@PathVariable(value = "id") Long id ){
		HashMap<String, Object> hmActor = new HashMap<String, Object>();
		Actor actor = actorRepository.findById(id).orElseThrow(null);
			
		actorRepository.delete(actor);
			
		hmActor.put("Message : ", "Delete Actor Succes");
		hmActor.put("Data : ", actor);
			
		return hmActor;
			
	}

}
