package com.example.MovieJBossToolsYuslam.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.example.MovieJBossToolsYuslam.repository.GenresRepository;
import com.example.MovieJBossToolsYuslam.dto.GenresDto;
import com.example.MovieJBossToolsYuslam.entities.Genres;

@RestController
@RequestMapping("/api")

public class GenresController {

	@Autowired
	GenresRepository genresRepository;
	
	//Get All Genres
	@GetMapping("/read/genres")
	public HashMap<String, Object> readGenres(){
		HashMap<String, Object> hmGenres = new HashMap<String, Object>();
		List<GenresDto> genresDtos = new ArrayList<GenresDto>();
		for(Genres genres : genresRepository.findAll()) {
			GenresDto genresDto = new GenresDto(genres.getGenId(), genres.getGenTitle());
			genresDtos.add(genresDto);
		}
			
		hmGenres.put("Message : ", "Read All Genres Success");
		hmGenres.put("Total : ", genresDtos.size());
		hmGenres.put("Data : " , genresDtos);
			
		return hmGenres;
		
		}
	
	//Create Genres
	@PostMapping("/create/genres")
	public HashMap<String, Object> createGenres(@Valid @RequestBody GenresDto genresDtoDetails){
		HashMap<String, Object> hmGenres = new HashMap<String, Object>();
		Genres genres = new Genres(genresDtoDetails.getGenId(), genresDtoDetails.getGenTitle());
				
		hmGenres.put("Message :", "Create Genres Succes");
		hmGenres.put("Data : ", genresRepository.save(genres));
			
		return hmGenres;
		
		}
	
	//Update Genres
	@PutMapping("/update/genres/{id}")
	public HashMap<String, Object> updateGenres(@PathVariable(value = "id") Long id, @Valid @RequestBody GenresDto genresDtoDetails){
		HashMap<String, Object> hmGenres = new HashMap<String, Object>();
		Genres genres = genresRepository.findById(id).orElseThrow(null);
			
		if(genresDtoDetails.getGenTitle() != null) {
			genres.setGenTitle(genresDtoDetails.getGenTitle());
		}
			
		genresRepository.save(genres);
			
		hmGenres.put("Message : ", "Update Genres Succes");
		hmGenres.put("Data : ", genres);
			
		return hmGenres;
		
		}
	
	//Delete Genres
	@DeleteMapping("/delete/genres/{id}")
	public HashMap<String, Object> deleteGenres(@PathVariable(value = "id") Long id ){
		HashMap<String, Object> hmGenres = new HashMap<String, Object>();
		Genres genres = genresRepository.findById(id).orElseThrow(null);
			
		genresRepository.delete(genres);
			
		hmGenres.put("Message : ", "Delete Genres Succes");
		hmGenres.put("Data :", genres);
			
		return hmGenres;
		
		}
	
}
