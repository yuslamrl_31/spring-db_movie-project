package com.example.MovieJBossToolsYuslam.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.example.MovieJBossToolsYuslam.repository.DirectorRepository;
import com.example.MovieJBossToolsYuslam.dto.DirectorDto;
import com.example.MovieJBossToolsYuslam.entities.Director;

@RestController
@RequestMapping("/api")
public class DirectorController {

	@Autowired
	DirectorRepository directorRepository;
	
	//Get All Director
	@GetMapping("/read/director")
	public HashMap<String, Object> readDirector(){
		HashMap<String, Object> hmDirector = new HashMap<String, Object>();
		List<DirectorDto> directorDtos = new ArrayList<DirectorDto>();
		for(Director director : directorRepository.findAll()) {				
			DirectorDto directorDto = new DirectorDto(director.getDirId(), director.getDirFname(), director.getDirLname());
			directorDtos.add(directorDto);
		}
				
		hmDirector.put("Message : ", "Read All Director Success");
		hmDirector.put("Total : ", directorDtos.size());
		hmDirector.put("Data : " , directorDtos);
				
		return hmDirector;
		
	}
	
	
	//Create Director
	@PostMapping("/create/director")
		public HashMap<String, Object> createDirector(@Valid @RequestBody DirectorDto directorDtoDetails){
		HashMap<String, Object> hmDirector = new HashMap<String, Object>();
		Director director = new Director(directorDtoDetails.getDirId(), directorDtoDetails.getDirFname(), directorDtoDetails.getDirLname());
			
		hmDirector.put("Message : ", "Create Director Succes");
		hmDirector.put("Data : ", directorRepository.save(director));
			
		return hmDirector;
			
	}
	
	//Update Director
	@PutMapping("/update/director/{id}")
	public HashMap<String, Object> updateDirector(@PathVariable(value = "id") Long id, @Valid @RequestBody DirectorDto directorDtoDetails){
		HashMap<String, Object> hmDirector = new HashMap<String, Object>();
		Director director = directorRepository.findById(id).orElseThrow(null);
			
		if(directorDtoDetails.getDirFname() != null) {
			director.setDirFname(directorDtoDetails.getDirFname());
		}
			
		if(directorDtoDetails.getDirLname() != null) {
			director.setDirLname(directorDtoDetails.getDirLname());
		}
			
		directorRepository.save(director);
			
		hmDirector.put("Message : ", "Updated Director Succes");
		hmDirector.put("Data : ", director);
			
		return hmDirector;
	}
	
	//Delete Director
	@DeleteMapping("/delete/director/{id}")
	public HashMap<String, Object> deleteDirector(@PathVariable(value = "id") Long id ){
		HashMap<String, Object> hmDirector = new HashMap<String, Object>();
		Director director = directorRepository.findById(id).orElseThrow(null);
			
		directorRepository.delete(director);
			
		hmDirector.put("Message : ", "Delete Director Succes");
		hmDirector.put("Data :", director);
			
		return hmDirector;
	}
	
}
