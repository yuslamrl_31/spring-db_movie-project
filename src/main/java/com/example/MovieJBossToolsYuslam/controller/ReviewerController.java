package com.example.MovieJBossToolsYuslam.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.example.MovieJBossToolsYuslam.repository.ReviewerRepository;
import com.example.MovieJBossToolsYuslam.dto.ReviewerDto;
import com.example.MovieJBossToolsYuslam.entities.Reviewer;

@RestController
@RequestMapping("/api")
public class ReviewerController {
	
	@Autowired
	ReviewerRepository reviewerRepository;
	
	//Read Reviewer
	@GetMapping("/read/reviewer")
	public HashMap<String, Object> readReviewer(){
		HashMap<String, Object> hmReviewer = new HashMap<String, Object>();
		List<ReviewerDto> reviewerDtos = new ArrayList<ReviewerDto>();
		for(Reviewer reviewer : reviewerRepository.findAll()) {
			ReviewerDto reviewerDto = new ReviewerDto(reviewer.getRevId(), reviewer.getRevName());
			reviewerDtos.add(reviewerDto);
		}
			
		hmReviewer.put("Message : ", "Read Reviewer Succes");
		hmReviewer.put("Total : ", reviewerDtos.size());
		hmReviewer.put("Data : ", reviewerDtos);
			
		return hmReviewer;
			
	}
	
	//Create Reviewer
	@PostMapping("/create/reviewer")
	public HashMap<String, Object> createReviewer(@Valid @RequestBody ReviewerDto reviewerDtoDetails){
		HashMap<String, Object> hmReviewer = new HashMap<String, Object>();
		Reviewer reviewer = new Reviewer(reviewerDtoDetails.getRevId(), reviewerDtoDetails.getRevName());
			
		hmReviewer.put("Message : ", "Create Reviewer Succes");
		hmReviewer.put("Data : ", reviewerRepository.save(reviewer));
			
		return hmReviewer;
	}
	
	//Update Reviewer
	@PutMapping("/update/reviewer/{id}")
	public HashMap<String, Object> updateReviewer(@PathVariable (value = "id") Long id, @Valid @RequestBody ReviewerDto reviewerDtoDetails ){
		HashMap<String, Object> hmReviewer = new HashMap<String, Object>();
		Reviewer reviewer = reviewerRepository.findById(id).orElseThrow(null);
			
		if(reviewerDtoDetails.getRevName() != null) {
			reviewer.setRevName(reviewerDtoDetails.getRevName());
		}
			
		reviewerRepository.save(reviewer);
			
		hmReviewer.put("Message : ", "Update Reviewer Succes");
		hmReviewer.put("Data : ", reviewer);
			
		return hmReviewer;
	
	}
	
	
	//Delete Reviewer
	@DeleteMapping("/delete/reviewer/{id}")
	public HashMap<String, Object> deleteReviewer(@PathVariable (value = "id") Long id){
		HashMap<String, Object> hmReviewer = new HashMap<String, Object>();
		Reviewer reviewer = reviewerRepository.findById(id).orElseThrow(null);
			
		reviewerRepository.delete(reviewer);
			
		hmReviewer.put("Message : ", "Delete Reviewer Succes");
		hmReviewer.put("Data : ", reviewer);
			
		return hmReviewer;
	
	}
}
