package com.example.MovieJBossToolsYuslam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieJBossToolsYuslamApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieJBossToolsYuslamApplication.class, args);
	}

}
