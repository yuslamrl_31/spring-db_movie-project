package com.example.MovieJBossToolsYuslam.entities;
// Generated Mar 24, 2020 4:48:09 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Movie generated by hbm2java
 */
@Entity
@Table(name = "movie", schema = "public")
public class Movie implements java.io.Serializable {

	private long movId;
	private String movTitle;
	private Integer movYear;
	private Integer movTime;
	private String movLang;
	private Date movDtRel;
	private String movRelCountry;
	private Set<Rating> ratings = new HashSet<Rating>(0);
	private Set<Director> directors = new HashSet<Director>(0);
	private Set<Genres> genreses = new HashSet<Genres>(0);
	private Set<MovieCast> movieCasts = new HashSet<MovieCast>(0);

	public Movie() {
	}

	public Movie(long movId, String movTitle) {
		this.movId = movId;
		this.movTitle = movTitle;
	}
	
	public Movie(long movId, String movTitle, Integer movYear, Integer movTime, String movLang, Date movDtRel,
			String movRelCountry) {
		super();
		this.movId = movId;
		this.movTitle = movTitle;
		this.movYear = movYear;
		this.movTime = movTime;
		this.movLang = movLang;
		this.movDtRel = movDtRel;
		this.movRelCountry = movRelCountry;
	}

	public Movie(long movId, String movTitle, Integer movYear, Integer movTime, String movLang, Date movDtRel,
			String movRelCountry, Set<Rating> ratings, Set<Director> directors, Set<Genres> genreses,
			Set<MovieCast> movieCasts) {
		this.movId = movId;
		this.movTitle = movTitle;
		this.movYear = movYear;
		this.movTime = movTime;
		this.movLang = movLang;
		this.movDtRel = movDtRel;
		this.movRelCountry = movRelCountry;
		this.ratings = ratings;
		this.directors = directors;
		this.genreses = genreses;
		this.movieCasts = movieCasts;
	}

	@Id

	@Column(name = "mov_id", unique = true, nullable = false)
	public long getMovId() {
		return this.movId;
	}

	public void setMovId(long movId) {
		this.movId = movId;
	}

	@Column(name = "mov_title", nullable = false, length = 50)
	public String getMovTitle() {
		return this.movTitle;
	}

	public void setMovTitle(String movTitle) {
		this.movTitle = movTitle;
	}

	@Column(name = "mov_year")
	public Integer getMovYear() {
		return this.movYear;
	}

	public void setMovYear(Integer movYear) {
		this.movYear = movYear;
	}

	@Column(name = "mov_time")
	public Integer getMovTime() {
		return this.movTime;
	}

	public void setMovTime(Integer movTime) {
		this.movTime = movTime;
	}

	@Column(name = "mov_lang", length = 50)
	public String getMovLang() {
		return this.movLang;
	}

	public void setMovLang(String movLang) {
		this.movLang = movLang;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "mov_dt_rel", length = 13)
	public Date getMovDtRel() {
		return this.movDtRel;
	}

	public void setMovDtRel(Date movDtRel) {
		this.movDtRel = movDtRel;
	}

	@Column(name = "mov_rel_country", length = 5)
	public String getMovRelCountry() {
		return this.movRelCountry;
	}

	public void setMovRelCountry(String movRelCountry) {
		this.movRelCountry = movRelCountry;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "movie")
	public Set<Rating> getRatings() {
		return this.ratings;
	}

	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "movies")
	public Set<Director> getDirectors() {
		return this.directors;
	}

	public void setDirectors(Set<Director> directors) {
		this.directors = directors;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "movie_genres", schema = "public", joinColumns = {
			@JoinColumn(name = "mov_id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "gen_id", nullable = false, updatable = false) })
	public Set<Genres> getGenreses() {
		return this.genreses;
	}

	public void setGenreses(Set<Genres> genreses) {
		this.genreses = genreses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "movie")
	public Set<MovieCast> getMovieCasts() {
		return this.movieCasts;
	}

	public void setMovieCasts(Set<MovieCast> movieCasts) {
		this.movieCasts = movieCasts;
	}

}
