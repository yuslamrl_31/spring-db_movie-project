package com.example.MovieJBossToolsYuslam.entities;
// Generated Mar 24, 2020 4:48:09 PM by Hibernate Tools 4.3.5.Final

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * MovieCastId generated by hbm2java
 */
@Embeddable
public class MovieCastId implements java.io.Serializable {

	private int actId;
	private long movId;

	public MovieCastId() {
	}

	public MovieCastId(int actId, long movId) {
		this.actId = actId;
		this.movId = movId;
	}

	@Column(name = "act_id", nullable = false)
	public int getActId() {
		return this.actId;
	}

	public void setActId(int actId) {
		this.actId = actId;
	}

	@Column(name = "mov_id", nullable = false)
	public long getMovId() {
		return this.movId;
	}

	public void setMovId(long movId) {
		this.movId = movId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof MovieCastId))
			return false;
		MovieCastId castOther = (MovieCastId) other;

		return (this.getActId() == castOther.getActId()) && (this.getMovId() == castOther.getMovId());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getActId();
		result = 37 * result + (int) this.getMovId();
		return result;
	}

}
