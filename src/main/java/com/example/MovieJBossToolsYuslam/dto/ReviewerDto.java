package com.example.MovieJBossToolsYuslam.dto;

public class ReviewerDto {

	private int revId;
	private String revName;
	
	public ReviewerDto() {
		
	}

	public ReviewerDto(int revId, String revName) {
		super();
		this.revId = revId;
		this.revName = revName;
	}

	public int getRevId() {
		return revId;
	}

	public void setRevId(int revId) {
		this.revId = revId;
	}

	public String getRevName() {
		return revName;
	}

	public void setRevName(String revName) {
		this.revName = revName;
	}
	
	
}
