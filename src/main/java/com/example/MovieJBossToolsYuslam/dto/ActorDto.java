package com.example.MovieJBossToolsYuslam.dto;

public class ActorDto {
	
	private int actId;
	
	private String actFname;
	
	private String actLname;
	
	private char actGender;
	
	public ActorDto() {
		
	}

	public ActorDto(int actId, String actFname, String actLname, char actGender) {
		super();
		this.actId = actId;
		this.actFname = actFname;
		this.actLname = actLname;
		this.actGender = actGender;
	}

	public int getActId() {
		return actId;
	}

	public void setActId(int actId) {
		this.actId = actId;
	}

	public String getActFname() {
		return actFname;
	}

	public void setActFname(String actFname) {
		this.actFname = actFname;
	}

	public String getActLname() {
		return actLname;
	}

	public void setActLname(String actLname) {
		this.actLname = actLname;
	}

	public char getActGender() {
		return actGender;
	}

	public void setActGender(char actGender) {
		this.actGender = actGender;
	}

	
	
	
	
}
